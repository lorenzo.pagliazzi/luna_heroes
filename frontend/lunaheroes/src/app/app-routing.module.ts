import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroTableComponent } from './hero-table/hero-table.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [

  {path: 'login', component: LoginComponent},
  {path: 'hero-table', component: HeroTableComponent},
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
