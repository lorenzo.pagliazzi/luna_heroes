/**
 * lunaheroes API
 * lunaheroes API documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface Hero { 
    age: number;
    gender: Hero.GenderEnum;
    heroName: string;
    id?: number;
    notes?: string;
    signs?: string;
}
export namespace Hero {
    export type GenderEnum = 'MALE' | 'FEMALE' | 'BINARY';
    export const GenderEnum = {
        MALE: 'MALE' as GenderEnum,
        FEMALE: 'FEMALE' as GenderEnum,
        BINARY: 'BINARY' as GenderEnum
    };
}


