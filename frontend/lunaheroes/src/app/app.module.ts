import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeroTableComponent, DeleteHeroDialog,NewHeroDialog,HeroDetailsDialog,EditHeroDialog } from './hero-table/hero-table.component';
/* Module to help Services over HTTP */
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button'
import { MatTableModule } from '@angular/material/table';
import { MatSliderModule} from '@angular/material/slider';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import '@angular/compiler';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeroTableComponent,
    DeleteHeroDialog,
    NewHeroDialog,
    HeroDetailsDialog,
    EditHeroDialog,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatTableModule,
    MatSliderModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatDialogModule,
  ],
  providers: [MatIconRegistry,],
  bootstrap: [AppComponent]
})
export class AppModule { }
