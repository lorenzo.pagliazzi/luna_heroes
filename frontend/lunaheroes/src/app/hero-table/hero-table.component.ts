import { HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Hero, HeroResourceService, UserJwtControllerService } from '../api';

@Component({
  selector: 'app-hero-table',
  templateUrl: './hero-table.component.html',
  styleUrls: ['./hero-table.component.css']
})
export class HeroTableComponent implements OnInit {

  displayedColumns: string[] = ["heroName", "age", "gender", "signs","action"];
  dataSource: Hero [] ;
  constructor(
    private heroes: HeroResourceService,
    private dialog: MatDialog,
  ) {
    this.heroes.defaultHeaders = new HttpHeaders()
                                      .set('activate','*/*')
                                      .set('Authorization',`Bearer ${localStorage.getItem('token')}`);
    this.update();
  }

  newHero(){
    let dialogref = this.dialog.open(NewHeroDialog);

    dialogref.afterClosed().subscribe((result)=> {
      if(result){
        this.heroes.createHeroUsingPOST(result).
          subscribe(response => {
              console.log(response);
              this.update();
          });
      }
    });
  }
  showHero(hero: Hero){
    console.log(hero.id.toString());
    let dialogref = this.dialog.open(HeroDetailsDialog, {
      data:{
        hero: hero,
      }
    });
  }
  deleteHero(id_hero: number){
    let dialogref = this.dialog.open(DeleteHeroDialog);
    dialogref.afterClosed().subscribe((answer: boolean) => {
      if(answer){
        console.log("need to delete DB-record!");
       this.heroes.deleteHeroUsingDELETE(id_hero)
          .subscribe(result => {
          console.log(result);
          this.update();
          });
      }
    });
  }
  editHero(_hero: Hero){
    let dialogref = this.dialog.open(EditHeroDialog, {
      data:{
        hero: _hero
      }
    });
    dialogref.afterClosed().subscribe(result => {
      console.log(result);
      if((_hero.age != result.age) || (_hero.heroName != result.heroName)
        || (_hero.notes != result.notes) || (_hero.signs != result.signs))
        console.log("Need to modify DB-record!");
        this.heroes.updateHeroUsingPUT(result).
          subscribe((result) => {
            console.log(result);
            this.update();
          });
    });
  }
  update(): void{
    this.heroes.getAllHeroesUsingGET("body").subscribe((result) => {
      console.log(result);
      this.dataSource = [...result];
      if(this.dataSource){
        console.log("data is fetched");
      }
      else
        console.log("something went wrong");
    });
  }
  ngOnInit(): void {
  }

}
/*********************************************** */
@Component({
  selector:'newhero-dialog',
  templateUrl: './newhero-dialog.html',
  styleUrls: []
})

export class NewHeroDialog {
  public hero: Hero
constructor(){
    this.hero = {
      heroName: '',
      age: 0,
      gender: Hero.GenderEnum.BINARY,
    }
}
}
/********************************************* */
@Component({
    selector:'herodetails-dialog',
    templateUrl: './herodetails-dialog.html',
    styleUrls: []
})

export class HeroDetailsDialog {
  heroName: string;
  heroAge: number;
  heroNotes: string;
  heroSigns: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    ){
      console.log(data.hero);
      this.heroName = data.hero.heroName;
      this.heroAge = data.hero.age;
      this.heroNotes = data.hero.notes;
      this.heroSigns = data.hero.signs;
    }
    
}
/********************************************* */
@Component({
  selector:'delethero-dialog',
  template: `<h1 mat-dialog-title>Do you want to proceed with deleting?</h1><div>
  <button mat-icon-button style="color: black;" [mat-dialog-close]="true" ><mat-icon>done</mat-icon></button>
  <button mat-icon-button style="color: red;" [mat-dialog-close]="false"><mat-icon>clear</mat-icon></button>
</div>`,
styles:[`div {margin-left:25%;}`],
})

export class DeleteHeroDialog {
constructor(){}
}

/*********************************************** */
@Component({
  selector:'edithero-dialog',
  templateUrl: './edithero-dialog.html',
  styleUrls: []
})
export class EditHeroDialog {
  public hero: Hero;
constructor(
  @Inject(MAT_DIALOG_DATA) private data: any,
){
  this.hero = {
    id: data.hero.id,
    heroName: data.hero.heroName,
    age: data.hero.age,
    gender: data.hero.gender,
    signs: data.hero.signs,
    notes: data.hero.notes
  };
}
}