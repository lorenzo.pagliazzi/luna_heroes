import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Configuration, UserJwtControllerService } from '../api';
import { LoginVM }  from '../api/model/loginVM';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: LoginVM;
  constructor(
    private userService: UserJwtControllerService,
    private router: Router,
    ) { 
    this.user = {
      username: '',
      password: '',
    };
  }

  login(){
      this.userService.authorizeUsingPOST(this.user, "body").subscribe(
        answer => {
          if(answer.id_token){
            this.userService.configuration.accessToken = answer.id_token;
            this.userService.configuration.password = this.user.password;
            this.userService.configuration.username = this.user.username;
            this.userService.defaultHeaders.append("Authorization", `Bearer ${answer.id_token}`);
            localStorage.setItem('token', answer.id_token);
            this.router.navigate(['/hero-table']);
          }});
  }

  ngOnInit(): void {
  }

}
