export interface ILunaUser {
  id?: number;
  userName?: string;
  userPwd?: string;
}

export class LunaUser implements ILunaUser {
  constructor(public id?: number, public userName?: string, public userPwd?: string) {}
}
