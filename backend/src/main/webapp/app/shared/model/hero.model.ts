import { Gender } from 'app/shared/model/enumerations/gender.model';

export interface IHero {
  id?: number;
  heroName?: string;
  age?: number;
  gender?: Gender;
  notes?: string;
  signs?: string;
}

export class Hero implements IHero {
  constructor(
    public id?: number,
    public heroName?: string,
    public age?: number,
    public gender?: Gender,
    public notes?: string,
    public signs?: string
  ) {}
}
