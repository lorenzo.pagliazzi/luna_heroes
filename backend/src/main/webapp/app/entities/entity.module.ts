import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'hero',
        loadChildren: () => import('./hero/hero.module').then(m => m.LunaheroesHeroModule),
      },
      {
        path: 'luna-user',
        loadChildren: () => import('./luna-user/luna-user.module').then(m => m.LunaheroesLunaUserModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class LunaheroesEntityModule {}
