import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LunaheroesSharedModule } from 'app/shared/shared.module';
import { LunaUserComponent } from './luna-user.component';
import { LunaUserDetailComponent } from './luna-user-detail.component';
import { LunaUserUpdateComponent } from './luna-user-update.component';
import { LunaUserDeleteDialogComponent } from './luna-user-delete-dialog.component';
import { lunaUserRoute } from './luna-user.route';

@NgModule({
  imports: [LunaheroesSharedModule, RouterModule.forChild(lunaUserRoute)],
  declarations: [LunaUserComponent, LunaUserDetailComponent, LunaUserUpdateComponent, LunaUserDeleteDialogComponent],
  entryComponents: [LunaUserDeleteDialogComponent],
})
export class LunaheroesLunaUserModule {}
