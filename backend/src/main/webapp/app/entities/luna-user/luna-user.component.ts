import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ILunaUser } from 'app/shared/model/luna-user.model';
import { LunaUserService } from './luna-user.service';
import { LunaUserDeleteDialogComponent } from './luna-user-delete-dialog.component';

@Component({
  selector: 'jhi-luna-user',
  templateUrl: './luna-user.component.html',
})
export class LunaUserComponent implements OnInit, OnDestroy {
  lunaUsers?: ILunaUser[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected lunaUserService: LunaUserService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.lunaUserService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<ILunaUser[]>) => (this.lunaUsers = res.body || []));
      return;
    }

    this.lunaUserService.query().subscribe((res: HttpResponse<ILunaUser[]>) => (this.lunaUsers = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInLunaUsers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ILunaUser): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInLunaUsers(): void {
    this.eventSubscriber = this.eventManager.subscribe('lunaUserListModification', () => this.loadAll());
  }

  delete(lunaUser: ILunaUser): void {
    const modalRef = this.modalService.open(LunaUserDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.lunaUser = lunaUser;
  }
}
