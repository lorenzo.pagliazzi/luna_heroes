import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILunaUser } from 'app/shared/model/luna-user.model';

@Component({
  selector: 'jhi-luna-user-detail',
  templateUrl: './luna-user-detail.component.html',
})
export class LunaUserDetailComponent implements OnInit {
  lunaUser: ILunaUser | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ lunaUser }) => (this.lunaUser = lunaUser));
  }

  previousState(): void {
    window.history.back();
  }
}
