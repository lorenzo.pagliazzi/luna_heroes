import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ILunaUser, LunaUser } from 'app/shared/model/luna-user.model';
import { LunaUserService } from './luna-user.service';
import { LunaUserComponent } from './luna-user.component';
import { LunaUserDetailComponent } from './luna-user-detail.component';
import { LunaUserUpdateComponent } from './luna-user-update.component';

@Injectable({ providedIn: 'root' })
export class LunaUserResolve implements Resolve<ILunaUser> {
  constructor(private service: LunaUserService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ILunaUser> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((lunaUser: HttpResponse<LunaUser>) => {
          if (lunaUser.body) {
            return of(lunaUser.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new LunaUser());
  }
}

export const lunaUserRoute: Routes = [
  {
    path: '',
    component: LunaUserComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'lunaheroesApp.lunaUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: LunaUserDetailComponent,
    resolve: {
      lunaUser: LunaUserResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'lunaheroesApp.lunaUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: LunaUserUpdateComponent,
    resolve: {
      lunaUser: LunaUserResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'lunaheroesApp.lunaUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: LunaUserUpdateComponent,
    resolve: {
      lunaUser: LunaUserResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'lunaheroesApp.lunaUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
