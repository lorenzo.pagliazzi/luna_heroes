import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILunaUser } from 'app/shared/model/luna-user.model';
import { LunaUserService } from './luna-user.service';

@Component({
  templateUrl: './luna-user-delete-dialog.component.html',
})
export class LunaUserDeleteDialogComponent {
  lunaUser?: ILunaUser;

  constructor(protected lunaUserService: LunaUserService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.lunaUserService.delete(id).subscribe(() => {
      this.eventManager.broadcast('lunaUserListModification');
      this.activeModal.close();
    });
  }
}
