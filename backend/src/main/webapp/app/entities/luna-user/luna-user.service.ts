import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { ILunaUser } from 'app/shared/model/luna-user.model';

type EntityResponseType = HttpResponse<ILunaUser>;
type EntityArrayResponseType = HttpResponse<ILunaUser[]>;

@Injectable({ providedIn: 'root' })
export class LunaUserService {
  public resourceUrl = SERVER_API_URL + 'api/luna-users';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/luna-users';

  constructor(protected http: HttpClient) {}

  create(lunaUser: ILunaUser): Observable<EntityResponseType> {
    return this.http.post<ILunaUser>(this.resourceUrl, lunaUser, { observe: 'response' });
  }

  update(lunaUser: ILunaUser): Observable<EntityResponseType> {
    return this.http.put<ILunaUser>(this.resourceUrl, lunaUser, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ILunaUser>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILunaUser[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILunaUser[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
