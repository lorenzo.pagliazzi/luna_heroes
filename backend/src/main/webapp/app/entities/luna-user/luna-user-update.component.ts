import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ILunaUser, LunaUser } from 'app/shared/model/luna-user.model';
import { LunaUserService } from './luna-user.service';

@Component({
  selector: 'jhi-luna-user-update',
  templateUrl: './luna-user-update.component.html',
})
export class LunaUserUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    userName: [null, [Validators.required]],
    userPwd: [null, [Validators.required, Validators.minLength(8)]],
  });

  constructor(protected lunaUserService: LunaUserService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ lunaUser }) => {
      this.updateForm(lunaUser);
    });
  }

  updateForm(lunaUser: ILunaUser): void {
    this.editForm.patchValue({
      id: lunaUser.id,
      userName: lunaUser.userName,
      userPwd: lunaUser.userPwd,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const lunaUser = this.createFromForm();
    if (lunaUser.id !== undefined) {
      this.subscribeToSaveResponse(this.lunaUserService.update(lunaUser));
    } else {
      this.subscribeToSaveResponse(this.lunaUserService.create(lunaUser));
    }
  }

  private createFromForm(): ILunaUser {
    return {
      ...new LunaUser(),
      id: this.editForm.get(['id'])!.value,
      userName: this.editForm.get(['userName'])!.value,
      userPwd: this.editForm.get(['userPwd'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILunaUser>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
