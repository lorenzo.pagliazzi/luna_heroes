package org.lunaheroes.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

import org.lunaheroes.domain.enumeration.Gender;

/**
 * A Hero.
 */
@Entity
@Table(name = "hero")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "hero")
public class Hero implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 5, max = 15)
    @Column(name = "hero_name", length = 15, nullable = false)
    private String heroName;

    @NotNull
    @Column(name = "age", nullable = false)
    private Integer age;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "gender", nullable = false)
    private Gender gender;

    @Size(max = 35)
    @Column(name = "notes", length = 35)
    private String notes;

    @Size(max = 10)
    @Column(name = "signs", length = 10)
    private String signs;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeroName() {
        return heroName;
    }

    public Hero heroName(String heroName) {
        this.heroName = heroName;
        return this;
    }

    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    public Integer getAge() {
        return age;
    }

    public Hero age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public Hero gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getNotes() {
        return notes;
    }

    public Hero notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSigns() {
        return signs;
    }

    public Hero signs(String signs) {
        this.signs = signs;
        return this;
    }

    public void setSigns(String signs) {
        this.signs = signs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Hero)) {
            return false;
        }
        return id != null && id.equals(((Hero) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Hero{" +
            "id=" + getId() +
            ", heroName='" + getHeroName() + "'" +
            ", age=" + getAge() +
            ", gender='" + getGender() + "'" +
            ", notes='" + getNotes() + "'" +
            ", signs='" + getSigns() + "'" +
            "}";
    }
}
