package org.lunaheroes.repository;

import org.lunaheroes.domain.LunaUser;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LunaUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LunaUserRepository extends JpaRepository<LunaUser, Long> {
}
