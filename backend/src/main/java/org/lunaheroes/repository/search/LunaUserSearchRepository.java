package org.lunaheroes.repository.search;

import org.lunaheroes.domain.LunaUser;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link LunaUser} entity.
 */
public interface LunaUserSearchRepository extends ElasticsearchRepository<LunaUser, Long> {
}
