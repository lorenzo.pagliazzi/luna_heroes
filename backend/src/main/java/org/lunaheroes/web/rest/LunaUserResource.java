package org.lunaheroes.web.rest;

import org.lunaheroes.domain.LunaUser;
import org.lunaheroes.repository.LunaUserRepository;
import org.lunaheroes.repository.search.LunaUserSearchRepository;
import org.lunaheroes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.lunaheroes.domain.LunaUser}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class LunaUserResource {

    private final Logger log = LoggerFactory.getLogger(LunaUserResource.class);

    private static final String ENTITY_NAME = "lunaUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LunaUserRepository lunaUserRepository;

    private final LunaUserSearchRepository lunaUserSearchRepository;

    public LunaUserResource(LunaUserRepository lunaUserRepository, LunaUserSearchRepository lunaUserSearchRepository) {
        this.lunaUserRepository = lunaUserRepository;
        this.lunaUserSearchRepository = lunaUserSearchRepository;
    }

    /**
     * {@code POST  /luna-users} : Create a new lunaUser.
     *
     * @param lunaUser the lunaUser to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new lunaUser, or with status {@code 400 (Bad Request)} if the lunaUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/luna-users")
    public ResponseEntity<LunaUser> createLunaUser(@Valid @RequestBody LunaUser lunaUser) throws URISyntaxException {
        log.debug("REST request to save LunaUser : {}", lunaUser);
        if (lunaUser.getId() != null) {
            throw new BadRequestAlertException("A new lunaUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LunaUser result = lunaUserRepository.save(lunaUser);
        lunaUserSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/luna-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /luna-users} : Updates an existing lunaUser.
     *
     * @param lunaUser the lunaUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated lunaUser,
     * or with status {@code 400 (Bad Request)} if the lunaUser is not valid,
     * or with status {@code 500 (Internal Server Error)} if the lunaUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/luna-users")
    public ResponseEntity<LunaUser> updateLunaUser(@Valid @RequestBody LunaUser lunaUser) throws URISyntaxException {
        log.debug("REST request to update LunaUser : {}", lunaUser);
        if (lunaUser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LunaUser result = lunaUserRepository.save(lunaUser);
        lunaUserSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, lunaUser.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /luna-users} : get all the lunaUsers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of lunaUsers in body.
     */
    @GetMapping("/luna-users")
    public List<LunaUser> getAllLunaUsers() {
        log.debug("REST request to get all LunaUsers");
        return lunaUserRepository.findAll();
    }

    /**
     * {@code GET  /luna-users/:id} : get the "id" lunaUser.
     *
     * @param id the id of the lunaUser to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the lunaUser, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/luna-users/{id}")
    public ResponseEntity<LunaUser> getLunaUser(@PathVariable Long id) {
        log.debug("REST request to get LunaUser : {}", id);
        Optional<LunaUser> lunaUser = lunaUserRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(lunaUser);
    }

    /**
     * {@code DELETE  /luna-users/:id} : delete the "id" lunaUser.
     *
     * @param id the id of the lunaUser to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/luna-users/{id}")
    public ResponseEntity<Void> deleteLunaUser(@PathVariable Long id) {
        log.debug("REST request to delete LunaUser : {}", id);
        lunaUserRepository.deleteById(id);
        lunaUserSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/luna-users?query=:query} : search for the lunaUser corresponding
     * to the query.
     *
     * @param query the query of the lunaUser search.
     * @return the result of the search.
     */
    @GetMapping("/_search/luna-users")
    public List<LunaUser> searchLunaUsers(@RequestParam String query) {
        log.debug("REST request to search LunaUsers for query {}", query);
        return StreamSupport
            .stream(lunaUserSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
