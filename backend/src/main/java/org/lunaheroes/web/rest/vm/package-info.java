/**
 * View Models used by Spring MVC REST controllers.
 */
package org.lunaheroes.web.rest.vm;
