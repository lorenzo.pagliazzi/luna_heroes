import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { LunaheroesTestModule } from '../../../test.module';
import { LunaUserComponent } from 'app/entities/luna-user/luna-user.component';
import { LunaUserService } from 'app/entities/luna-user/luna-user.service';
import { LunaUser } from 'app/shared/model/luna-user.model';

describe('Component Tests', () => {
  describe('LunaUser Management Component', () => {
    let comp: LunaUserComponent;
    let fixture: ComponentFixture<LunaUserComponent>;
    let service: LunaUserService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [LunaheroesTestModule],
        declarations: [LunaUserComponent],
      })
        .overrideTemplate(LunaUserComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LunaUserComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(LunaUserService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new LunaUser(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.lunaUsers && comp.lunaUsers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
