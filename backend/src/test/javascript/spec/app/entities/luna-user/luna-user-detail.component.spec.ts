import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LunaheroesTestModule } from '../../../test.module';
import { LunaUserDetailComponent } from 'app/entities/luna-user/luna-user-detail.component';
import { LunaUser } from 'app/shared/model/luna-user.model';

describe('Component Tests', () => {
  describe('LunaUser Management Detail Component', () => {
    let comp: LunaUserDetailComponent;
    let fixture: ComponentFixture<LunaUserDetailComponent>;
    const route = ({ data: of({ lunaUser: new LunaUser(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [LunaheroesTestModule],
        declarations: [LunaUserDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(LunaUserDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(LunaUserDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load lunaUser on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.lunaUser).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
