package org.lunaheroes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.lunaheroes.web.rest.TestUtil;

public class LunaUserTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LunaUser.class);
        LunaUser lunaUser1 = new LunaUser();
        lunaUser1.setId(1L);
        LunaUser lunaUser2 = new LunaUser();
        lunaUser2.setId(lunaUser1.getId());
        assertThat(lunaUser1).isEqualTo(lunaUser2);
        lunaUser2.setId(2L);
        assertThat(lunaUser1).isNotEqualTo(lunaUser2);
        lunaUser1.setId(null);
        assertThat(lunaUser1).isNotEqualTo(lunaUser2);
    }
}
