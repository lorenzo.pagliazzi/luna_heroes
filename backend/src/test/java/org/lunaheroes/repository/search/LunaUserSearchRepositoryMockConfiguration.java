package org.lunaheroes.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link LunaUserSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class LunaUserSearchRepositoryMockConfiguration {

    @MockBean
    private LunaUserSearchRepository mockLunaUserSearchRepository;

}
