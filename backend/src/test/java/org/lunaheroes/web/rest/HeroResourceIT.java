package org.lunaheroes.web.rest;

import org.lunaheroes.LunaheroesApp;
import org.lunaheroes.domain.Hero;
import org.lunaheroes.repository.HeroRepository;
import org.lunaheroes.repository.search.HeroSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.lunaheroes.domain.enumeration.Gender;
/**
 * Integration tests for the {@link HeroResource} REST controller.
 */
@SpringBootTest(classes = LunaheroesApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class HeroResourceIT {

    private static final String DEFAULT_HERO_NAME = "AAAAAAAAAA";
    private static final String UPDATED_HERO_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_AGE = 1;
    private static final Integer UPDATED_AGE = 2;

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    private static final String DEFAULT_SIGNS = "AAAAAAAAAA";
    private static final String UPDATED_SIGNS = "BBBBBBBBBB";

    @Autowired
    private HeroRepository heroRepository;

    /**
     * This repository is mocked in the org.lunaheroes.repository.search test package.
     *
     * @see org.lunaheroes.repository.search.HeroSearchRepositoryMockConfiguration
     */
    @Autowired
    private HeroSearchRepository mockHeroSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHeroMockMvc;

    private Hero hero;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hero createEntity(EntityManager em) {
        Hero hero = new Hero()
            .heroName(DEFAULT_HERO_NAME)
            .age(DEFAULT_AGE)
            .gender(DEFAULT_GENDER)
            .notes(DEFAULT_NOTES)
            .signs(DEFAULT_SIGNS);
        return hero;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hero createUpdatedEntity(EntityManager em) {
        Hero hero = new Hero()
            .heroName(UPDATED_HERO_NAME)
            .age(UPDATED_AGE)
            .gender(UPDATED_GENDER)
            .notes(UPDATED_NOTES)
            .signs(UPDATED_SIGNS);
        return hero;
    }

    @BeforeEach
    public void initTest() {
        hero = createEntity(em);
    }

    @Test
    @Transactional
    public void createHero() throws Exception {
        int databaseSizeBeforeCreate = heroRepository.findAll().size();
        // Create the Hero
        restHeroMockMvc.perform(post("/api/heroes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isCreated());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeCreate + 1);
        Hero testHero = heroList.get(heroList.size() - 1);
        assertThat(testHero.getHeroName()).isEqualTo(DEFAULT_HERO_NAME);
        assertThat(testHero.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testHero.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testHero.getNotes()).isEqualTo(DEFAULT_NOTES);
        assertThat(testHero.getSigns()).isEqualTo(DEFAULT_SIGNS);

        // Validate the Hero in Elasticsearch
        verify(mockHeroSearchRepository, times(1)).save(testHero);
    }

    @Test
    @Transactional
    public void createHeroWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = heroRepository.findAll().size();

        // Create the Hero with an existing ID
        hero.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHeroMockMvc.perform(post("/api/heroes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isBadRequest());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeCreate);

        // Validate the Hero in Elasticsearch
        verify(mockHeroSearchRepository, times(0)).save(hero);
    }


    @Test
    @Transactional
    public void checkHeroNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = heroRepository.findAll().size();
        // set the field null
        hero.setHeroName(null);

        // Create the Hero, which fails.


        restHeroMockMvc.perform(post("/api/heroes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isBadRequest());

        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAgeIsRequired() throws Exception {
        int databaseSizeBeforeTest = heroRepository.findAll().size();
        // set the field null
        hero.setAge(null);

        // Create the Hero, which fails.


        restHeroMockMvc.perform(post("/api/heroes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isBadRequest());

        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGenderIsRequired() throws Exception {
        int databaseSizeBeforeTest = heroRepository.findAll().size();
        // set the field null
        hero.setGender(null);

        // Create the Hero, which fails.


        restHeroMockMvc.perform(post("/api/heroes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isBadRequest());

        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHeroes() throws Exception {
        // Initialize the database
        heroRepository.saveAndFlush(hero);

        // Get all the heroList
        restHeroMockMvc.perform(get("/api/heroes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hero.getId().intValue())))
            .andExpect(jsonPath("$.[*].heroName").value(hasItem(DEFAULT_HERO_NAME)))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)))
            .andExpect(jsonPath("$.[*].signs").value(hasItem(DEFAULT_SIGNS)));
    }
    
    @Test
    @Transactional
    public void getHero() throws Exception {
        // Initialize the database
        heroRepository.saveAndFlush(hero);

        // Get the hero
        restHeroMockMvc.perform(get("/api/heroes/{id}", hero.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(hero.getId().intValue()))
            .andExpect(jsonPath("$.heroName").value(DEFAULT_HERO_NAME))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES))
            .andExpect(jsonPath("$.signs").value(DEFAULT_SIGNS));
    }
    @Test
    @Transactional
    public void getNonExistingHero() throws Exception {
        // Get the hero
        restHeroMockMvc.perform(get("/api/heroes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHero() throws Exception {
        // Initialize the database
        heroRepository.saveAndFlush(hero);

        int databaseSizeBeforeUpdate = heroRepository.findAll().size();

        // Update the hero
        Hero updatedHero = heroRepository.findById(hero.getId()).get();
        // Disconnect from session so that the updates on updatedHero are not directly saved in db
        em.detach(updatedHero);
        updatedHero
            .heroName(UPDATED_HERO_NAME)
            .age(UPDATED_AGE)
            .gender(UPDATED_GENDER)
            .notes(UPDATED_NOTES)
            .signs(UPDATED_SIGNS);

        restHeroMockMvc.perform(put("/api/heroes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedHero)))
            .andExpect(status().isOk());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);
        Hero testHero = heroList.get(heroList.size() - 1);
        assertThat(testHero.getHeroName()).isEqualTo(UPDATED_HERO_NAME);
        assertThat(testHero.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testHero.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testHero.getNotes()).isEqualTo(UPDATED_NOTES);
        assertThat(testHero.getSigns()).isEqualTo(UPDATED_SIGNS);

        // Validate the Hero in Elasticsearch
        verify(mockHeroSearchRepository, times(1)).save(testHero);
    }

    @Test
    @Transactional
    public void updateNonExistingHero() throws Exception {
        int databaseSizeBeforeUpdate = heroRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHeroMockMvc.perform(put("/api/heroes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isBadRequest());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Hero in Elasticsearch
        verify(mockHeroSearchRepository, times(0)).save(hero);
    }

    @Test
    @Transactional
    public void deleteHero() throws Exception {
        // Initialize the database
        heroRepository.saveAndFlush(hero);

        int databaseSizeBeforeDelete = heroRepository.findAll().size();

        // Delete the hero
        restHeroMockMvc.perform(delete("/api/heroes/{id}", hero.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Hero in Elasticsearch
        verify(mockHeroSearchRepository, times(1)).deleteById(hero.getId());
    }

    @Test
    @Transactional
    public void searchHero() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        heroRepository.saveAndFlush(hero);
        when(mockHeroSearchRepository.search(queryStringQuery("id:" + hero.getId())))
            .thenReturn(Collections.singletonList(hero));

        // Search the hero
        restHeroMockMvc.perform(get("/api/_search/heroes?query=id:" + hero.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hero.getId().intValue())))
            .andExpect(jsonPath("$.[*].heroName").value(hasItem(DEFAULT_HERO_NAME)))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)))
            .andExpect(jsonPath("$.[*].signs").value(hasItem(DEFAULT_SIGNS)));
    }
}
