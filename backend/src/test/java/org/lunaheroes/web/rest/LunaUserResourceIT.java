package org.lunaheroes.web.rest;

import org.lunaheroes.LunaheroesApp;
import org.lunaheroes.domain.LunaUser;
import org.lunaheroes.repository.LunaUserRepository;
import org.lunaheroes.repository.search.LunaUserSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LunaUserResource} REST controller.
 */
@SpringBootTest(classes = LunaheroesApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class LunaUserResourceIT {

    private static final String DEFAULT_USER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_USER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_USER_PWD = "AAAAAAAAAA";
    private static final String UPDATED_USER_PWD = "BBBBBBBBBB";

    @Autowired
    private LunaUserRepository lunaUserRepository;

    /**
     * This repository is mocked in the org.lunaheroes.repository.search test package.
     *
     * @see org.lunaheroes.repository.search.LunaUserSearchRepositoryMockConfiguration
     */
    @Autowired
    private LunaUserSearchRepository mockLunaUserSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLunaUserMockMvc;

    private LunaUser lunaUser;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LunaUser createEntity(EntityManager em) {
        LunaUser lunaUser = new LunaUser()
            .userName(DEFAULT_USER_NAME)
            .userPwd(DEFAULT_USER_PWD);
        return lunaUser;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LunaUser createUpdatedEntity(EntityManager em) {
        LunaUser lunaUser = new LunaUser()
            .userName(UPDATED_USER_NAME)
            .userPwd(UPDATED_USER_PWD);
        return lunaUser;
    }

    @BeforeEach
    public void initTest() {
        lunaUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createLunaUser() throws Exception {
        int databaseSizeBeforeCreate = lunaUserRepository.findAll().size();
        // Create the LunaUser
        restLunaUserMockMvc.perform(post("/api/luna-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lunaUser)))
            .andExpect(status().isCreated());

        // Validate the LunaUser in the database
        List<LunaUser> lunaUserList = lunaUserRepository.findAll();
        assertThat(lunaUserList).hasSize(databaseSizeBeforeCreate + 1);
        LunaUser testLunaUser = lunaUserList.get(lunaUserList.size() - 1);
        assertThat(testLunaUser.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testLunaUser.getUserPwd()).isEqualTo(DEFAULT_USER_PWD);

        // Validate the LunaUser in Elasticsearch
        verify(mockLunaUserSearchRepository, times(1)).save(testLunaUser);
    }

    @Test
    @Transactional
    public void createLunaUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lunaUserRepository.findAll().size();

        // Create the LunaUser with an existing ID
        lunaUser.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLunaUserMockMvc.perform(post("/api/luna-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lunaUser)))
            .andExpect(status().isBadRequest());

        // Validate the LunaUser in the database
        List<LunaUser> lunaUserList = lunaUserRepository.findAll();
        assertThat(lunaUserList).hasSize(databaseSizeBeforeCreate);

        // Validate the LunaUser in Elasticsearch
        verify(mockLunaUserSearchRepository, times(0)).save(lunaUser);
    }


    @Test
    @Transactional
    public void checkUserNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = lunaUserRepository.findAll().size();
        // set the field null
        lunaUser.setUserName(null);

        // Create the LunaUser, which fails.


        restLunaUserMockMvc.perform(post("/api/luna-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lunaUser)))
            .andExpect(status().isBadRequest());

        List<LunaUser> lunaUserList = lunaUserRepository.findAll();
        assertThat(lunaUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUserPwdIsRequired() throws Exception {
        int databaseSizeBeforeTest = lunaUserRepository.findAll().size();
        // set the field null
        lunaUser.setUserPwd(null);

        // Create the LunaUser, which fails.


        restLunaUserMockMvc.perform(post("/api/luna-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lunaUser)))
            .andExpect(status().isBadRequest());

        List<LunaUser> lunaUserList = lunaUserRepository.findAll();
        assertThat(lunaUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLunaUsers() throws Exception {
        // Initialize the database
        lunaUserRepository.saveAndFlush(lunaUser);

        // Get all the lunaUserList
        restLunaUserMockMvc.perform(get("/api/luna-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lunaUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
            .andExpect(jsonPath("$.[*].userPwd").value(hasItem(DEFAULT_USER_PWD)));
    }
    
    @Test
    @Transactional
    public void getLunaUser() throws Exception {
        // Initialize the database
        lunaUserRepository.saveAndFlush(lunaUser);

        // Get the lunaUser
        restLunaUserMockMvc.perform(get("/api/luna-users/{id}", lunaUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(lunaUser.getId().intValue()))
            .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
            .andExpect(jsonPath("$.userPwd").value(DEFAULT_USER_PWD));
    }
    @Test
    @Transactional
    public void getNonExistingLunaUser() throws Exception {
        // Get the lunaUser
        restLunaUserMockMvc.perform(get("/api/luna-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLunaUser() throws Exception {
        // Initialize the database
        lunaUserRepository.saveAndFlush(lunaUser);

        int databaseSizeBeforeUpdate = lunaUserRepository.findAll().size();

        // Update the lunaUser
        LunaUser updatedLunaUser = lunaUserRepository.findById(lunaUser.getId()).get();
        // Disconnect from session so that the updates on updatedLunaUser are not directly saved in db
        em.detach(updatedLunaUser);
        updatedLunaUser
            .userName(UPDATED_USER_NAME)
            .userPwd(UPDATED_USER_PWD);

        restLunaUserMockMvc.perform(put("/api/luna-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLunaUser)))
            .andExpect(status().isOk());

        // Validate the LunaUser in the database
        List<LunaUser> lunaUserList = lunaUserRepository.findAll();
        assertThat(lunaUserList).hasSize(databaseSizeBeforeUpdate);
        LunaUser testLunaUser = lunaUserList.get(lunaUserList.size() - 1);
        assertThat(testLunaUser.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testLunaUser.getUserPwd()).isEqualTo(UPDATED_USER_PWD);

        // Validate the LunaUser in Elasticsearch
        verify(mockLunaUserSearchRepository, times(1)).save(testLunaUser);
    }

    @Test
    @Transactional
    public void updateNonExistingLunaUser() throws Exception {
        int databaseSizeBeforeUpdate = lunaUserRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLunaUserMockMvc.perform(put("/api/luna-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lunaUser)))
            .andExpect(status().isBadRequest());

        // Validate the LunaUser in the database
        List<LunaUser> lunaUserList = lunaUserRepository.findAll();
        assertThat(lunaUserList).hasSize(databaseSizeBeforeUpdate);

        // Validate the LunaUser in Elasticsearch
        verify(mockLunaUserSearchRepository, times(0)).save(lunaUser);
    }

    @Test
    @Transactional
    public void deleteLunaUser() throws Exception {
        // Initialize the database
        lunaUserRepository.saveAndFlush(lunaUser);

        int databaseSizeBeforeDelete = lunaUserRepository.findAll().size();

        // Delete the lunaUser
        restLunaUserMockMvc.perform(delete("/api/luna-users/{id}", lunaUser.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LunaUser> lunaUserList = lunaUserRepository.findAll();
        assertThat(lunaUserList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the LunaUser in Elasticsearch
        verify(mockLunaUserSearchRepository, times(1)).deleteById(lunaUser.getId());
    }

    @Test
    @Transactional
    public void searchLunaUser() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        lunaUserRepository.saveAndFlush(lunaUser);
        when(mockLunaUserSearchRepository.search(queryStringQuery("id:" + lunaUser.getId())))
            .thenReturn(Collections.singletonList(lunaUser));

        // Search the lunaUser
        restLunaUserMockMvc.perform(get("/api/_search/luna-users?query=id:" + lunaUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lunaUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
            .andExpect(jsonPath("$.[*].userPwd").value(hasItem(DEFAULT_USER_PWD)));
    }
}
